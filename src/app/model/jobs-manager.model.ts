export class Industry {
    public id : number
    public name : String


}
export class Company {
    public id : number
    public name : String
    public industry : String
}

export class CompanyDto {
    public name : String
    public industry : number
}
export class JobDto {
    public title : String
    public description : String
    public seniority : String
    public salary : Number
    public company : Number
}

export class Job {
    public id : number
    public title : String
    public description : String
    public seniority : String
    public salary : Number
    public company : String
}
