import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Company } from '../model/jobs-manager.model';
import { CompanyService } from '../service/company.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {


  companies:Company[]


  constructor(private router:Router,
              private formBuilder: FormBuilder,
              private companyService: CompanyService,) { 
    
  }
  

  ngOnInit(): void {
    this.getCompanies()
  }

  openNewCompany(){
    this.router.navigateByUrl("/new-company")
  }

  getCompanies(){
    this.companyService.findAll().subscribe(
      result =>{
        this.companies=result
  

      }
    )
  }

}
