import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CompanyDto, Industry } from '../model/jobs-manager.model';
import { CompanyService } from '../service/company.service';
import { IndustryService } from '../service/industry.service';

@Component({
  selector: 'app-new-company',
  templateUrl: './new-company.component.html',
  styleUrls: ['./new-company.component.css']
})
export class NewCompanyComponent implements OnInit {

  industries:Industry[];
  companies:CompanyDto[];

  NewCompany = this.formBuilder.group({
    companyName: [],
    industryName: []})

  constructor(private router:Router,
              private industryService:IndustryService,
              private companyService:CompanyService, 
              private formBuilder:FormBuilder) { }

  ngOnInit(): void {
    this.getIndustries()
  }

  getIndustries(){
    this.industryService.findAll().subscribe(
      result => {
        this.industries=result;
      }
    )
  }

  save(){
    let company = new CompanyDto()
    company.name=this.NewCompany.value.companyName
    company.industry=this.NewCompany.value.industryName

    this.companyService.save(company).subscribe(result => this.companies.push(company))
     this.router.navigateByUrl("/list-company")
  }
}
