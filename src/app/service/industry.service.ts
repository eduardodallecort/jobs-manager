import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WS_INDUSTRY } from './endpoints';
import { HttpClient } from "@angular/common/http";
import { Industry } from "../model/jobs-manager.model";

@Injectable({
  providedIn: 'root'
})
export class IndustryService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<Industry[]>{
    return this.http.get<Industry[]>(WS_INDUSTRY)
  }

}
