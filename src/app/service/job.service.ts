import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Job, JobDto } from "../model/jobs-manager.model";
import { WS_JOBS, WS_JOBS_BY_COMPANY, WS_JOBS_BY_INDUSTRY } from './endpoints';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<Job[]>{
    return this.http.get<Job[]>(WS_JOBS)
  }

  findByIndustry(industryId:Number) : Observable<Job[]>{
    let url= WS_JOBS_BY_INDUSTRY + industryId
    return this.http.get<Job[]>(url)
  }

  findByCompany(companyId:Number) : Observable<Job[]>{
    let url= WS_JOBS_BY_COMPANY + companyId
    return this.http.get<Job[]>(url)
  }
  save(job:JobDto):Observable<JobDto>{
    return this.http.post<JobDto>(WS_JOBS,job)
    }


}
