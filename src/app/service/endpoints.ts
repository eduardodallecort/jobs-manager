const URL_BASE= "http://localhost:8080"

export const WS_INDUSTRY = URL_BASE + "/industry"

export const WS_COMPANY = URL_BASE + "/company"
export const WS_COMPANY_BY_INDUSTRY = WS_COMPANY + "/byIndustry/"


export const WS_JOBS = URL_BASE + "/job"
export const WS_JOBS_BY_COMPANY = WS_JOBS + "/byCompany/"
export const WS_JOBS_BY_INDUSTRY = WS_JOBS + "/byIndustry/"