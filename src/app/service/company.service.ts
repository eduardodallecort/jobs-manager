import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Company, CompanyDto } from "../model/jobs-manager.model";
import { WS_COMPANY, WS_COMPANY_BY_INDUSTRY } from './endpoints';


@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

    findByIndustry(industryId:number) : Observable<Company[]>{
      let url= WS_COMPANY_BY_INDUSTRY + industryId
      return this.http.get<Company[]>(url)
    }

    findAll():Observable<Company[]>{
      return this.http.get<Company[]>(WS_COMPANY)
    }

    save(company:CompanyDto):Observable<CompanyDto>{
    return this.http.post<CompanyDto>(WS_COMPANY,company)
    }
  }



