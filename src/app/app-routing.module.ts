import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyListComponent } from './company-list/company-list.component';
import { JobListComponent } from './job-list/job-list.component';
import { NewCompanyComponent } from './new-company/new-company.component';
import { NewJobComponent } from './new-job/new-job.component';

const routes: Routes = [
  {path:"company-list", component: CompanyListComponent},
  {path:"new-company", component: NewCompanyComponent},
  {path:"job-list", component: JobListComponent},
  {path:"new-job", component: NewJobComponent},
  {path:"",pathMatch:"full",redirectTo:"/job-list"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


