import { Component, OnInit } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { Router } from '@angular/router';
import { Company, JobDto } from '../model/jobs-manager.model';
import { CompanyService } from '../service/company.service';
import { JobService } from '../service/job.service';

@Component({
  selector: 'app-new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.css']
})
export class NewJobComponent implements OnInit {

  companies:Company[]
  jobs:JobDto[]
  

  NewJob = this.formBuilder.group({
    jobName: [],
    description: [],
    seniority:[],
    salary:[],
    company:[]
    })

  constructor(private router:Router,
              private formBuilder:FormBuilder,
              private companyService:CompanyService,
              private jobService:JobService) { }

  ngOnInit(): void {
    this.getCompanies()
  }

  getCompanies(){
    this.companyService.findAll().subscribe(
      result =>{
        this.companies=result

      }
    )
  }
  save(){
    let job = new JobDto()

    job.title=this.NewJob.value.jobName
    job.description=this.NewJob.value.description
    job.seniority=this.NewJob.value.seniority
    job.salary=this.NewJob.value.salary
    job.company=this.NewJob.value.company
  
    this.jobService.save(job).subscribe(result => this.jobs.push(job))
    this.router.navigateByUrl("/job-list")
    
  }

}
