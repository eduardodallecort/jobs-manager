import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Company, Industry, Job } from '../model/jobs-manager.model';
import { CompanyService } from '../service/company.service';
import { IndustryService } from '../service/industry.service';
import { JobService } from '../service/job.service';

@Component({
    selector: 'app-job-list',
    templateUrl: './job-list.component.html',
    styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {

    industries: Industry[]
    companies: Company[]
    jobs: Job[]

    formListJobs = this.formBuilder.group({
        industry: [''],
        company: ['']
    })

    constructor(private router: Router, 
                private formBuilder: FormBuilder,
                private industryService: IndustryService,
                private companyService: CompanyService,
                private jobService: JobService) { }

    ngOnInit(): void {
        this.getIndustries()
    }

    openNewJob() {
        this.router.navigateByUrl("/new-job")
    }

    getIndustries() {
        this.industryService.findAll().subscribe(
            result => {
                this.industries = result
            }
        )
    }

    getCompanies() {
        let companyId = this.formListJobs.value.industry
        this.companyService.findByIndustry(companyId).subscribe(
            result => {
                this.companies = result
                this.formListJobs.patchValue({
                    company: ''
                })
            }
        )
    }

    findJobs() {
        let industryId = this.formListJobs.value.industry
        let companyId = this.formListJobs.value.company

        let serviceResult: Observable<Job[]>

        if (companyId !== '') {
            serviceResult = this.jobService.findByCompany(companyId)
        } else {
            serviceResult = this.jobService.findByIndustry(industryId)
        }
        
        serviceResult.subscribe(
            result => {
                this.jobs = result
            }
        )
    }
}
